class Exame{

  String nome;
  String valor;

  Exame({this.nome, this.valor});

  Exame.fromJson(Map<String, dynamic> json)
      : nome = json['nome'],
        valor = json['valor'];

  Map<String, dynamic> toJson() =>
  {
    'nome': nome,
    'valor': valor,
  };

}