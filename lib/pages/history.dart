import 'dart:convert';

import 'package:exame_calc/helpers/file_handler_history.dart';
import 'package:exame_calc/helpers/history_card.dart';
import 'package:flutter/material.dart';
import 'package:exame_calc/helpers/exame_obj.dart';
import 'package:exame_calc/helpers/history_obj.dart';

void main() => runApp(MaterialApp(
  home: History(),
));

class History extends StatefulWidget{
  @override
  _QuoteListState createState() => _QuoteListState();
}


class _QuoteListState extends State <History> {

  List<String> examHistory = [ ];

  List<HistoryObj> historyObj = new List<HistoryObj>();

  FileHandlerHistory fileHandlerHistory = new FileHandlerHistory();

  void readExamesFile() async
  {
    examHistory = await fileHandlerHistory.readHistory();
    for(var i=0; i< examHistory.length-1;i++)
    {
      List<Exame> exames = (jsonDecode(examHistory[i]) as List).map((e) => new Exame.fromJson(e)).toList();


      num valorTotalDia = 0;
      List<String> tmp = new List<String>();
      for(var k=0;k<exames.length-1;k++)
      {
        print('Exame: '+exames[k].nome);
        tmp.add("\n"+exames[k].nome+" ("+exames[k].valor+" €)");
        valorTotalDia += num.parse(exames[k].valor);
      }

      print('Tag: '+exames[exames.length-1].nome.toString());
      print('Data: '+exames[exames.length-1].valor.toString());
      print('Valor Total: '+valorTotalDia.toStringAsFixed(2));

      HistoryObj obj = new HistoryObj(
        tag: exames[exames.length-1].nome.toString(),
        total: valorTotalDia.toStringAsFixed(2),
        date: exames[exames.length-1].valor.toString(),
        exames: tmp,
      );
      historyObj.insert(i,obj);

    }

    setState(() {

    });
  }

  @override
  void initState() {
    super.initState();
    readExamesFile();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('History'),
        centerTitle: true,
        backgroundColor: Colors.redAccent,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () => Navigator.popAndPushNamed(context, '/'),
        ),
      ),
      body: ListView(
      children: historyObj.map((histobj) => HistoryCard(
        tag: histobj.tag,
        total: histobj.total,
        date: histobj.date,
        exames: histobj.exames,
        delete: () {
        setState(() { });  }
      ) ).toList(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Cuidado!"),
            content: new Text("Deseja mesmo apagar todo o historico?"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Sim"),
                onPressed: () async {
                  await fileHandlerHistory.clearHistory();
                  Navigator.of(context).pop();
                  Navigator.popAndPushNamed(context, '/');
                },
              ),
              new FlatButton(
                child: new Text("Não"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
          },
        );
        },
        backgroundColor: Colors.red[600],
        tooltip: 'Increment',
        child: Icon(Icons.delete),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

