import 'package:flutter/material.dart';

class HistoryCard extends StatelessWidget {

  final String tag;
  final String date;
  final String total;
  final List<String> exames;

  final Function delete;
  HistoryCard({ this.tag, this.date, this.total, this.exames, this.delete});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(4, 4, 4, 0),
      child: Padding(
        padding: const EdgeInsets.all(2.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              tag,
              style: TextStyle(
                fontSize: 14.0,
                color: Colors.grey[600],

              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 4),
            Text(
              date+'\n'+
              total+' €',
              style: TextStyle(
                fontSize: 12.0,
                color: Colors.grey[800],
              ),
              textAlign: TextAlign.center,
            ),
            FlatButton.icon(
            onPressed: (){
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  // return object of type Dialog
                  return AlertDialog(
                    title: new Text("Exames Realizados"),
                    content: Row(
                        children: <Widget>[
                          Flexible(
                            child: SingleChildScrollView(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Text(exames.toString())
                                    ],
                              ),
                            ),
                          ),
                        ],
                    ),
                  );
                },
              );
            },
            icon: Icon(Icons.info),
            label: Text('Mais detalhe')),
          ],
        ),
      ),
    );
  }
}