import 'package:flutter/material.dart';
import 'package:exame_calc/pages/home.dart';
import 'package:exame_calc/pages/config.dart';
import 'package:exame_calc/pages/history.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  initialRoute: '/',
  routes: {
    '/': (context) => Home(),
    '/config': (context) => Config(),
    '/history': (context) => History(),
  },
));

