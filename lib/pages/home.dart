import 'dart:convert';
import 'dart:math';
import 'package:exame_calc/helpers/file_handler_history.dart';
import 'package:flutter/services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter/material.dart';
import 'package:exame_calc/helpers/exame_obj.dart';
import 'package:exame_calc/helpers/exames_home.dart';
import 'package:exame_calc/helpers/file_handler.dart';

void main() => runApp(MaterialApp(
  home: Home(),
));

class Home extends StatefulWidget{
  @override
  _QuoteListState createState() => _QuoteListState();
}

class _QuoteListState extends State <Home> {

  List<Exame> exames = [];

  List<Exame> exames_efectuados = [];

  FileHandler fileHandler = new FileHandler();
  FileHandlerHistory fileHandlerHistory = new FileHandlerHistory();

  num valorTotal = 0.0;

  String sessionName = "";

  void readExamesFile() async
  {
    exames = await fileHandler.readExames();
    exames.sort((a, b) { return a.nome.toLowerCase().compareTo(b.nome.toLowerCase()); });
    setState(() {
      exames.toString();
    });

    List<String> history = [];
    history =  await fileHandlerHistory.readHistory();
    print("history: "+history.toString());
  }

  double dp(double val, int places){
    double mod = pow(10.0, places);
    return ((val * mod).round().toDouble() / mod);
  }

  @override
  void initState() {
    super.initState();
    readExamesFile();
    //String baseData = '[{"nome":"Tiróide ARS","valor":"6.94"},{"nome":"Tiróide ADSE","valor":"6.44"},{"nome":"Tiróide Advancecare","valor":"13.95"},{"nome":"Tiróide Multicare","valor":"13.5"},{"nome":"Tiroide Medis","valor":"12.38"},{"nome":"Tiróide Allianz","valor":"17.1"},{"nome":"Tiróide SAM","valor":"14.27"},{"nome":"Abdómen ARS","valor":"9.76"},{"nome":"Abdominal ADSE","valor":"9.05"},{"nome":"Abdominal Medis","valor":"13.5"},{"nome":"Abdominal Advancecare","valor":"13.95"},{"nome":"Abdominal Multicare","valor":"14.63"},{"nome":"Abdominal cartão cliente","valor":"20.25"},{"nome":"Carotídeo ARS","valor":"10.11"},{"nome":"Carotídeo ADSE","valor":"10.43"},{"nome":"Carotídeo Medis","valor":"22.5"},{"nome":"Carotídeo Multicare","valor":"22.5"},{"nome":"Carotídeo Advancecare","valor":"36"},{"nome":"Carotídeo cartão cliente","valor":"63"},{"nome":"Renal ARS","valor":"9.78"},{"nome":"Renal ADSE","valor":"9.07"},{"nome":"Renal Advancecare","valor":"14.4"},{"nome":"Renal Multicare","valor":"14.63"},{"nome":"Renal SAMS","valor":"20.25"},{"nome":"Vesical ARS","valor":"6.72"},{"nome":"Vesical ADSE","valor":"6.23"},{"nome":"Vesical Advancecare","valor":"11.25"},{"nome":"Vesical Multicare","valor":"13.5"},{"nome":"Abdómen + Renal + Vesical Medis","valor":"22.78"}]';
    //exames = (jsonDecode(baseData) as List).map((e) => new Exame.fromJson(e)).toList();
    //fileHandler.writeExames(exames);
    print("InitState Home");
  }

  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Gravar dia:'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  TextField(
                    autofocus: true,
                    decoration: new InputDecoration(
                        labelText: 'Tag do dia: ', hintText: 'ex. Alfena'),
                    onChanged: (value) {
                      sessionName = value;
                    },
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Gravar'),
                onPressed: () async {
                  var now = new DateTime.now();
                  exames_efectuados.add(Exame(nome: sessionName, valor: now.toString()));
                  await fileHandlerHistory.writeHistory(exames_efectuados);
                  setState(() {
                    valorTotal = 0;
                    exames_efectuados.clear();
                  });
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('Cancelar'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  Widget _getFAB() {
    return SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: IconThemeData(size: 22),
        backgroundColor: Colors.red,
        visible: true,
        curve: Curves.bounceIn,
        children: [
          // FAB 1
          SpeedDialChild(
              child: Icon(Icons.delete),
              backgroundColor:  Colors.red,
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    // return object of type Dialog
                    return AlertDialog(
                      title: new Text("Cuidado!"),
                      content: new Text("Deseja mesmo apagar todos os valores?"),
                      actions: <Widget>[
                        // usually buttons at the bottom of the dialog
                        new FlatButton(
                          child: new Text("Sim"),
                          onPressed: () {

                            setState(() {
                              valorTotal = 0;
                              exames_efectuados.clear();
                            });

                            Navigator.of(context).pop();
                          },
                        ),
                        new FlatButton(
                          child: new Text("Não"),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );

              },
              label: 'Limpar',
              labelStyle: TextStyle(
              fontWeight: FontWeight.w500,
              color: Colors.white,
              fontSize: 16.0),
              labelBackgroundColor: Colors.red
          ),
          // FAB 2
          SpeedDialChild(
              child: Icon(Icons.save),
              backgroundColor:  Colors.red,
              onTap: () {
                _displayDialog(context);
              },
              label: 'Guardar',
              labelStyle: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                  fontSize: 16.0),
              labelBackgroundColor: Colors.red
          ),
          // FAB 3
          SpeedDialChild(
              child: Icon(Icons.history),
              backgroundColor:  Colors.red,
              onTap: () {
                String json = jsonEncode(exames_efectuados);
                print('Exames Efectuados: '+json);
                Navigator.popAndPushNamed(context, '/history');
              },
              label: 'Histórico',
              labelStyle: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                  fontSize: 16.0),
              labelBackgroundColor: Colors.red
          ),
        ],
    );
  }


        @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('Exame Calculator App'),
        centerTitle: true,
        backgroundColor: Colors.redAccent,
        leading: new IconButton(
          icon: new Icon(Icons.settings),
          onPressed: () {
            Navigator.popAndPushNamed(context, '/config');
          },
        ),
      ),
      body:  Column(
        children: <Widget>[
          Expanded(
            flex: 14,
            child:  GridView.count(
              crossAxisCount: 4,
              children: exames.map((exame) => ExameCard(
                exameObj: exame,
                add: () {
                  setState(() {

                    HapticFeedback.vibrate();
                    Feedback.forTap(context);
                    print('Add :'+exame.valor.toString());
                    exames_efectuados.add(exame);
                    valorTotal += num.parse(exame.valor);
                  });
                },
                remove: () {
                  setState(() {
                    print('Remove :'+exame.valor.toString());
                    exames_efectuados.remove(exame);
                    if(valorTotal > 0) valorTotal -= num.parse(exame.valor);
                  });
                }
            ) ).toList(),
            ),
          ),
          SizedBox(height: 5),
          Expanded(
            flex: 2,
            child:  Container(
              padding: const EdgeInsets.all(8.0),
               decoration: BoxDecoration(
                 border: Border.all(
                   color: Colors.grey[400]
                 )
               ),
                child: Text(
                  'Valor total: '+valorTotal.toStringAsFixed(2)+' €\n'
                  'Valor Final: '+(valorTotal*0.75).toStringAsFixed(2)+' €',
                  style: TextStyle(
                    height: 1.5,
                    fontSize: 16.0
                  ),
                ),
            ),
          ),

        ],
      ),
      floatingActionButton: _getFAB(),
    );
  }
}

