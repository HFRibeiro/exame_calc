import 'package:flutter/material.dart';
import 'exame_obj.dart';

class ExameCard extends StatelessWidget {

  final Exame exameObj;
  final Function add, remove;
  ExameCard({ this.exameObj, this.add, this.remove});

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 5,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              exameObj.nome,
              style: TextStyle(
                fontSize: 10.0,
                color: Colors.grey[600],
                height: 1.2,
              ),
              textAlign: TextAlign.center,
            ),
            Text(
              exameObj.valor + ' €',
              style: TextStyle(
                fontSize: 10.0,
                color: Colors.grey[800],
                height: 1,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              padding: new EdgeInsets.fromLTRB(0,2,0,5),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 6,
                      child:
                    Material(
                        child: Ink(
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.brown, width: 2.0),
                            color: Colors.red,
                            shape: BoxShape.circle,
                          ),
                          child: InkWell(
                            //This keeps the splash effect within the circle
                            borderRadius: BorderRadius.circular(10.0), //Something large to ensure a circle
                            onTap: remove ,
                            child: Padding(
                              padding:EdgeInsets.all(0.0),
                              child: Icon(
                                Icons.remove,
                                size: 15.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                    ),
                  ),
                  Expanded(
                    flex: 8,
                      child:  Material(
                          child: Ink(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.green[800], width: 2.0),
                              color: Colors.green,
                              shape: BoxShape.circle,
                            ),
                            child: InkWell(
                              //This keeps the splash effect within the circle
                              borderRadius: BorderRadius.circular(10.0), //Something large to ensure a circle
                              onTap: add ,
                              child: Padding(
                                padding:EdgeInsets.all(0.0),
                                child: Icon(
                                  Icons.add,
                                  size: 30.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                      ),
                  ),
                ],
              ),
            ),
          ],
        ),
    );
  }
}