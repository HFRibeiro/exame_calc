import 'package:flutter/material.dart';
import 'package:exame_calc/helpers/exame_obj.dart';
import 'package:exame_calc/helpers/quote_card.dart';
import 'package:exame_calc/helpers/file_handler.dart';

void main() => runApp(MaterialApp(
  home: Config(),
));

class Config extends StatefulWidget{
  @override
  _QuoteListState createState() => _QuoteListState();
}


class _QuoteListState extends State <Config> {

  List<Exame> exames = [ ];

  String eName = "";
  String eValor = "";

  FileHandler fileHandler = new FileHandler();

  void readExamesFile() async
  {
    exames = await fileHandler.readExames();
    exames.sort((a, b) { return a.nome.toLowerCase().compareTo(b.nome.toLowerCase()); });
    setState(() {
      print(exames.toString());
    });
  }

  @override
  void initState() {
    super.initState();
    readExamesFile();
  }


  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Inserir novo exame:'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  TextField(
                    autofocus: true,
                    decoration: new InputDecoration(
                        labelText: 'Nome do exame: ', hintText: 'ex. Tiróide ADSE'),
                    onChanged: (value) {
                      eName = value;
                    },
                  ),
                  TextField(
                    autofocus: true,
                    decoration: new InputDecoration(
                        labelText: 'Valor do exame: ', hintText: 'ex. 6.44'),
                    onChanged: (value) {
                      eValor = value;
                    },
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Adicionar'),
                onPressed: () {
                  exames.add(Exame(nome: eName,valor: eValor));
                  fileHandler.writeExames(exames);
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('Cancelar'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('Configuration'),
        centerTitle: true,
        backgroundColor: Colors.redAccent,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () => Navigator.popAndPushNamed(context, '/'),
        ),
      ),
      body: ListView(
        children: exames.map((exame) => QuoteCard(
            exameObj: exame,
            delete: () {
              setState(() {
                exames.remove(exame);
                fileHandler.writeExames(exames);
              });
            }
        ) ).toList(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _displayDialog(context),
        backgroundColor: Colors.red[600],
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

