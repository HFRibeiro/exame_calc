import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:exame_calc/helpers/exame_obj.dart';

class FileHandlerHistory {

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future get _localFile async {
    final path = await _localPath;
    return File('$path/history.txt');
  }

  Future writeHistory(List<Exame> exames) async {
    final file = await _localFile;
    String json = jsonEncode(exames);
    return file.writeAsString(json+";", mode: FileMode.append);//, mode: FileMode.append
  }

  Future clearHistory() async {
    final file = await _localFile;
    return file.writeAsString("");//, mode: FileMode.append
  }

  Future<List> readHistory() async {
    try {
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();
      List<String> spliced = contents.split(";");
      return spliced;
    } catch (e) {
      print(e);
      List<String> history = [ ];
      return history;
    }
  }
}