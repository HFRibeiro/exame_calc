import 'package:flutter/material.dart';
import 'exame_obj.dart';

class QuoteCard extends StatelessWidget {

  final Exame exameObj;
  final Function delete;
  QuoteCard({ this.exameObj, this.delete});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(4, 4, 4, 0),
      child: Padding(
        padding: const EdgeInsets.all(2.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              exameObj.nome,
              style: TextStyle(
                fontSize: 14.0,
                color: Colors.grey[600],

              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 4),
            Text(
              exameObj.valor.toString()+' €',
              style: TextStyle(
                fontSize: 12.0,
                color: Colors.grey[800],
              ),
              textAlign: TextAlign.center,
            ),
            FlatButton.icon(
            onPressed: (){
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  // return object of type Dialog
                  return AlertDialog(
                    title: new Text("Cuidado!"),
                    content: new Text("Deseja mesmo apagar?"),
                    actions: <Widget>[
                      // usually buttons at the bottom of the dialog
                      new FlatButton(
                        child: new Text("Sim"),
                        onPressed: () {
                          delete();
                          Navigator.of(context).pop();
                        },
                      ),
                      new FlatButton(
                        child: new Text("Não"),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            },
            icon: Icon(Icons.delete),
            label: Text('Delete')),
          ],
        ),
      ),
    );
  }
}