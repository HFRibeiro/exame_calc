import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:exame_calc/helpers/exame_obj.dart';

class FileHandler {

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future get _localFile async {
    final path = await _localPath;
    return File('$path/exame_calculator.txt');
  }

  Future writeExames(List exames) async {
    final file = await _localFile;
    String json = jsonEncode(exames);
    //print('saving: '+json);
    return file.writeAsString(json);
  }

  Future<List> readExames() async {
    try {
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();
      //print('File: '+contents);
      var myThing = (jsonDecode(contents) as List).map((e) => new Exame.fromJson(e)).toList();
      return myThing;
    } catch (e) {
      print(e);
      List<Exame> exames = [ ];
      return exames;
    }
  }
}